1.01

2011-04-25	修正 固定交叉报表 获取字段条件时产生的错误，
                修正 固定交叉报表 复制单元格格式计算纵坐标的错误

2011-04-06      解决,忽略POI生成xlsx文件font chartset不兼容的问题

1.02
2012-05-25      修改了open.report.calculate表达式计算等方法的返回类型，从Object改称BigDecimal,表达式计算支持字符串
2012-10-25      修改了交叉表中没有设定列或行头排序时,分组汇总数据不正确的bug
2012-12-05      同步更新common1.05包,交叉表效率有所提高